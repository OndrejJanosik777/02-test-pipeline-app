FROM continuumio/miniconda3:latest

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

RUN mkdir -p /backend

COPY ./backend /backend

RUN /opt/conda/bin/conda env create -f /backend/requirements.yml

ENV PATH /opt/conda/envs/02-test-pipeline-app/bin:$PATH
RUN echo "source activate 02-test-pipeline-app" >~/.bashrc

WORKDIR /backend